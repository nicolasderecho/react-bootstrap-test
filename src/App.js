import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import NotFoundPage from "./pages/NotFoundPage";
import HomePage from "./pages/HomePage";

function App() {
  return (
    <div>
      <Router>
        <Switch>
          <Route exact path="/" render={ (props) => <HomePage {...props} /> }/>
          <Route exact render={ (props) => <NotFoundPage {...props} /> }/>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
